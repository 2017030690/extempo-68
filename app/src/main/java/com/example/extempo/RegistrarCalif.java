package com.example.extempo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import database.Alumno;
import database.Calificacion;

public class RegistrarCalif extends AppCompatActivity {
    private final static String[] materias = { "MATERIAS", "Español", "Matematicas"};
    private Spinner cmbMateria;
    private String materia;
    private EditText txtMatricula;
    private EditText txtCalif;
    private Button btnGuardar;
    private Button btnListar;
    private Button btnRegresar;
    private Calificacion db;
    private String grupo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_calif);


        Bundle datos = getIntent().getExtras();
        grupo = datos.getString("seleccion");

        txtMatricula = (EditText) findViewById(R.id.txtMatricula);
        txtCalif = (EditText) findViewById(R.id.txtCalif);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnListar = (Button) findViewById(R.id.btnListar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);


        ArrayAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, materias);
        cmbMateria = (Spinner) findViewById(R.id.cmbMateria);
        cmbMateria.setAdapter(adapter);
        cmbMateria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                materia = cmbMateria.getSelectedItem().toString(); //Se guarda la materia seleccionada en el combobox
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //nueva bd
        this.db = new Calificacion(RegistrarCalif.this);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (materia.equals("MATERIA")) {
                    Toast.makeText(RegistrarCalif.this, "Favor de seleccionar una materia", Toast.LENGTH_SHORT).show();
                }

                else {

                    if (verificarCampos()) {
                        String matricula = txtMatricula.getText().toString();
                        String calificacion = txtCalif.getText().toString();

                        db.openDatabase();
                        Alumno alumno = new Alumno(matricula, calificacion, materia, grupo);
                        if (db.insertarCalif(alumno) > 0) {
                            Toast.makeText(RegistrarCalif.this, "Calificaciones guardadas", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(RegistrarCalif.this, "Error al guardar", Toast.LENGTH_SHORT).show();
                        }

                        db.cerrar();
                        limpiarCampos();
                    }
                }
            }
        });


        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegistrarCalif.this, ListActivity.class);

                //Enviar un dato String
                intent.putExtra("grupo", grupo);

                startActivityForResult(intent, 0);
            }
        });


        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    public boolean verificarCampos(){
        boolean b = true;
        if(txtMatricula.getText().toString().trim().equals("")){
            Toast.makeText(RegistrarCalif.this, "Favor de escribir la matricula",Toast.LENGTH_SHORT).show();
            txtMatricula.requestFocus();
            b = false;
        }
        else if(txtCalif.getText().toString().trim().equals("")){
            Toast.makeText(RegistrarCalif.this, "Favor de escribir una calificacion",Toast.LENGTH_SHORT).show();
            txtCalif.requestFocus();
            b = false;
        }

        return b;
    }

    public void limpiarCampos(){
        this.txtMatricula.setText("");
        this.txtCalif.setText("");
    }


}