package database;

import android.provider.BaseColumns;

public class DefinirTabla {

    public DefinirTabla() {
    }

    public static abstract class Alumno implements BaseColumns {
        public static final String TABLE_NAME = "alumnos";
        public static final String MATRICULA = "matricula";
        public static final String CALIFICACION = "calificacion";
        public static final String MATERIA = "materia";
        public static final String GRUPO = "grupo";
    }
}
