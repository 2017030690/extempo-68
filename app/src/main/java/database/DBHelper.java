package database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DBHelper extends SQLiteOpenHelper {
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA = " ,";
    private static final String SQL_CREATE_ALUMNO = " CREATE TABLE " +
            DefinirTabla.Alumno.TABLE_NAME + " (" +
            DefinirTabla.Alumno._ID + " INTEGER PRIMARY KEY, " +
            DefinirTabla.Alumno.MATRICULA + TEXT_TYPE + COMMA +
            DefinirTabla.Alumno.CALIFICACION + TEXT_TYPE + COMMA +
            DefinirTabla.Alumno.MATERIA + TEXT_TYPE + COMMA +
            DefinirTabla.Alumno.GRUPO + TEXT_TYPE + ")";
    private static final String SQL_DELETE_ALUMNO = "DROP TABLE IF EXISTS " +
            DefinirTabla.Alumno.TABLE_NAME;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "alumnos.db";

    //Constructor
    public DBHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //Métodos sobreescritos
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ALUMNO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ALUMNO);
        onCreate(db);
    }
}
